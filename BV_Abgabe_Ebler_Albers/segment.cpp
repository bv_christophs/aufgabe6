#define _CRT_SECURE_NO_WARNINGS

#include "image-io.h"
#include "segment.h"
#include <stdio.h>
#include<conio.h>
#include <stdlib.h>
#include<cmath>
#include <string>

void SegMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	fflush(stdin);
	char eingabe = '0';
	bool exit = false;
	int marker[MAXXDIM][MAXYDIM];
	

	do {
		int anz = 0;
		
		int pixelCount = 0;
		system("cls");
		printf("\nWas wollen sie tun ?\n");

		printf("1 = Blob Colloring\n");

		printf("2 = Blob Colloring Grauwerte\n");

		printf("3 = \n");

		printf("4 = \n");

		printf("5 = \n");

		

		printf("0 = zum Hauptmenue \n");



		printf("ihre Wahl ?:");
		fflush(stdin);

		scanf("%c", &eingabe);

		printf("\n");

		switch (eingabe) {
		case '0':
			exit = true;
			break;
		case'2':
			system("cls");
			printf("====================Blob Analyse Fuer Grauwerte ============================\n");
			printf("Schwellwert ? :");
			scanf("%i", &anz);


			if (anz > 0)
			{
				blob_colorring_grayscale(inImg, marker, anz);
			}
			
			break;
		case '1':
			system("cls");
			printf("====================Blob Analyse  ============================\n");
			printf("Schwellwert ? :");
			scanf("%i", &anz);


			if (anz > 0)
			{
				blob_colorring(inImg, marker, anz);
			}

			break;

		}





	} while (exit == false);


}

void replaceLable(int marker[MAXXDIM][MAXYDIM], int oldLable,int newLable)
{
	for (int x = 0; x < MAXXDIM; x++)
		for (int y = 0; y < MAXYDIM; y++)
			if (marker[x][y] == oldLable)
				marker[x][y] = newLable;
}

int  find_ex( int iMatrix[MAXXDIM][MAXYDIM])
{
	int ext = 0;
	for (int i = 0; i < MAXXDIM; i++)
		for (int j = 0; j < MAXYDIM; j++)
		{
			int value = abs(iMatrix[i][j]);
			if ( value > ext)
				ext = value;
		}
	return ext;
}


void blob_colorring_grayscale(unsigned char inImg[MAXXDIM][MAXYDIM],int  markerImg[MAXXDIM][MAXYDIM],unsigned char schwellwert)
{

	
	for (int y = 0;y < MAXYDIM;y++)
	{
		for (int x = 0;x < MAXXDIM;x++)
		{
			markerImg[y][x] = 0;
		}
	}
	
	int region =0;



	//for (int x = 1;x<MAXXDIM;x++)
	for (int y = 1;y < MAXYDIM;y++)
	{
		
		//for (int y = 1;y < MAXYDIM;y++)
		for (int x = 1;x<MAXXDIM;x++)
		{
		
			

			int divX = abs((int)inImg[y][x] - (int)inImg[y][x-1]);
			int divY = abs((int)inImg[y][x] - (int)inImg[y-1][x]);

			if (((divX > schwellwert)&(divY > schwellwert)))
			{
				region +=1;
				markerImg[y][x] = region;
				//My_IMGDEBUG(markerImg);
			}

			if ((divX > schwellwert)&(divY <= schwellwert))
			{
				
					markerImg[y][x] = markerImg[y-1][x];
			}

			if ((divX <= schwellwert)&(divY > schwellwert))
			{
					markerImg[y][x] = markerImg[y][x-1];

			
			}

			if ((divX <= schwellwert)&(divY <= schwellwert))
			{	
			
				int ex = 0;				if ((schwellwert / 2) < 1)				{					ex = 1;				}				else 				{				 ex = (schwellwert / 2);				}					 
				if (markerImg[x - 1][y] != markerImg[x][y - 1] &&(int)abs((int)inImg[y][x] - (int)inImg[y][x - ex]) <= schwellwert && (int)abs((int)inImg[y][x] - (int)inImg[y - ex][x]) <= schwellwert&& x > ex && y > ex) 				{
					int oldLab = 0;					if (markerImg[y - 1][x] > markerImg[y][x - 1])					{						markerImg[y - 1][x];					}					else					{						markerImg[y][x - 1];					}										int newLab = 0;					if (markerImg[y - 1][x] >markerImg[y][x - 1])					{						markerImg[y][x - 1];					}					else					{						markerImg[y - 1][x];					}
					replaceLable(markerImg, oldLab, newLab);					
				}

					markerImg[y][x] = markerImg[y - 1][x];
				
			}

		}

	}

	int objCount = 0;

	
	unsigned char	visio[MAXXDIM][MAXYDIM];
	initImmage(visio);
	
	


	int maximum = find_ex(markerImg);
	int null = 0;	
	for (int i = 0; i <= maximum; i++) {
		float counter = 0;		float value = 0;		float mw = 0;
		for (int x = 0; x < MAXXDIM; x++)		{
			for (int y = 0; y < MAXYDIM; y++)			{
				if (markerImg[x][y] == i)				{
					value += (float)inImg[x][y];
					counter += 1.0;
				}			}		}
		if (counter > 0)
			mw = value/ counter;
		if (mw != 0.0) {
			for (int x = 0; x < MAXXDIM; x++)			{
				for (int y = 0; y < MAXYDIM; y++)				{
					if (markerImg[x][y] == i)					{
						visio[x][y] = (unsigned char)mw;					}				}			}
		}
		else		{
			null++;		}
	}

	printf("Anzahl der Blobs = %i", maximum-null);
	
	//printf("anz = %i", objCount);
	_getch();
	My_IMGDEBUG(visio);
	//writeImage_ppm(visio, MAXXDIM, MAXYDIM);
	
}


void blob_colorring(unsigned char inImg[MAXXDIM][MAXYDIM], int  markerImg[MAXXDIM][MAXYDIM], unsigned char schwellwert)
{


	for (int y = 0;y < MAXYDIM;y++)
	{
		for (int x = 0;x < MAXXDIM;x++)
		{
			markerImg[y][x] = 0;
		}
	}

	unsigned char region = 0;



	//for (int x = 1;x<MAXXDIM;x++)
	for (int y = 1;y < MAXYDIM;y++)
	{

		//for (int y = 1;y < MAXYDIM;y++)
		for (int x = 1;x<MAXXDIM;x++)
		{



			int divX = abs((int)inImg[y][x] - (int)inImg[y][x - 1]);
			int divY = abs((int)inImg[y][x] - (int)inImg[y - 1][x]);

			if (((divX > schwellwert)&(divY > schwellwert)))
			{
				region += 1;
				markerImg[y][x] = region;
				//My_IMGDEBUG(markerImg);
			}

			if ((divX > schwellwert)&(divY < schwellwert))
			{

				markerImg[y][x] = markerImg[y - 1][x];
			}

			if ((divX < schwellwert)&(divY > schwellwert))
			{
				markerImg[y][x] = markerImg[y][x - 1];

			}

			if ((divX < schwellwert)&(divY < schwellwert))
			{

				if (markerImg[y][x - 1] < markerImg[y - 1][x])
				{
					markerImg[y][x] = markerImg[y][x - 1];
					region -= 1;
					for (int i = 1;i < MAXYDIM;i++)
					{
						for (int j = 1;j < MAXYDIM;j++)
						{
							if (markerImg[i][j] == markerImg[y - 1][x])
							{
								markerImg[i][j] = markerImg[y][x - 1];

							}

						}
					}

				}

				if (markerImg[y - 1][x] <markerImg[y][x - 1])
				{
					markerImg[y][x] = markerImg[y - 1][x];
					region -= 1;
					for (int i = 1;i < MAXYDIM;i++)
					{
						for (int j = 1;j < MAXYDIM;j++)
						{
							if (markerImg[i][j] == markerImg[y][x - 1])
							{
								markerImg[i][j] = markerImg[y - 1][x];

							}

						}
					}

				}


				if (markerImg[y][x - 1] == markerImg[y - 1][x])
				{

					markerImg[y][x] = markerImg[y - 1][x];
				}
			}

		}

	}

	int objCount = 0;

	for (int i = 1;i < MAXYDIM;i++)
	{
		for (int j = 1;j < MAXYDIM;j++)
		{
			if (markerImg[i][j] >objCount)
			{
				objCount = markerImg[i][j];
			}
		}
	}

	unsigned char	visio[MAXXDIM][MAXYDIM];
	initImmage(visio);
	int faktor = 1;
	if (objCount > 0)
	{
		faktor = PIXEL_DP / objCount;
	}

	for (int i = 1;i < MAXYDIM;i++)
	{
		for (int j = 1;j < MAXYDIM;j++)
		{
			visio[i][j] = markerImg[i][j] * faktor;
		}
	}


	printf("anz = %i", objCount);
	_getch();
	My_IMGDEBUG(visio);
	//writeImage_ppm(markerImg, MAXXDIM, MAXYDIM);

}