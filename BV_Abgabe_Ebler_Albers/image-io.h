#define MAXXDIM 256
#define MAXYDIM 256

#define PIXEL_DP 255

extern int readImage_ppm(unsigned char img[MAXXDIM][MAXYDIM]);
extern int writeImage_ppm(unsigned char img[MAXXDIM][MAXYDIM], int xdim, int ydim);
extern void viewImage_ppm(void);

extern void getFilename(char fname[30]);

extern void MY_viewImage_ppm(char fname[30]);
extern int My_readImage_ppm(char Filename[30], unsigned char img[MAXXDIM][MAXYDIM]);
extern int My_writeImage_ppm(char Filename[30], unsigned char img[MAXXDIM][MAXYDIM], int xdim, int ydim);

extern void My_IMGDEBUG(unsigned char img[MAXXDIM][MAXYDIM]);

extern void initImmage(unsigned char INimg[MAXXDIM][MAXYDIM]);
extern void initImmageWhite(unsigned char INimg[MAXXDIM][MAXYDIM]);
extern void initImmageGray(unsigned char INimg[MAXXDIM][MAXYDIM]);

extern void copyImmage(unsigned char INimg[MAXXDIM][MAXYDIM], unsigned char OUTimg[MAXXDIM][MAXYDIM]);
