#pragma once

// Fuer Grassfire
#define GF_TRIGGERLEVEL  200 // Schwellwert ab wann ein pixel als weiss erkannt wird
#define GF_WHITE 255 // komplett weiss
#define GF_BLACK 0 // schwatz

#define GF_NOTUSED 0  // Das Pixel wurde noch nie bearbeitet 
#define GF_INPROCESS 125 // Das Pixel ist aktiv und wird im n�chsten durchlauf bearbeitet
#define GF_DONE 255 // Das pixel ist komplett abgearbeitet und wird nicht mehr beachtet



extern void BagMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM]);

extern void schwellwert(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char  out[MAXXDIM][MAXYDIM], unsigned char schwellwert);

extern int glob_countWhitePixel(unsigned char img[MAXXDIM][MAXYDIM]);

extern int grasfire_Streifen(unsigned char inImg[MAXXDIM][MAXYDIM],  unsigned char start, unsigned char stop, bool debug);


extern bool Classifier(float volume, int columwith);

extern int CountBags(unsigned char inImg[MAXXDIM][MAXYDIM], int verteilung[100], int size,bool debug);

extern void create_report(int verteilung[100], int anzahl);