#pragma once
#define PIXEL_DEPTH 255

#define MAX 1
#define MIN 0


extern void init_cMatrix(unsigned char cMatrix[MAXXDIM][MAXYDIM], unsigned char val);
extern void init_iMatrix(int iMatrix[MAXXDIM][MAXYDIM]);
extern int  find_abs_extremum_iMatrix(int min_max, int iMatrix[MAXXDIM][MAXYDIM]);
extern void reset_blob_label(int iIMG[MAXXDIM][MAXYDIM], int oldLabel, int newLabel);
extern void blob_coloring_imagesensitiv(unsigned char img[MAXXDIM][MAXYDIM], unsigned char img2[MAXXDIM][MAXYDIM], int iIMG[MAXXDIM][MAXYDIM], int bereich, int keine_fransen, int writeImage);