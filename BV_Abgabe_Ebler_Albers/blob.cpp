#include "image-io.h"
#include <stdio.h>
#include <conio.h>
#include <Windows.h>
#include <stdlib.h>
#include <math.h>
#include "blob.h"




/***********************************************************************************************************/
/*                      Sonstige Hilfsfunktionen                                                           */
/***********************************************************************************************************/
void init_cMatrix(unsigned char cMatrix[MAXXDIM][MAXYDIM], unsigned char val)
{
	unsigned char init = val > PIXEL_DEPTH - 1 ? 255 : val < 0 ? 0 : val;
	for (int i = 0; i < MAXXDIM; i++)
		for (int j = 0; j < MAXYDIM; j++)
			cMatrix[i][j] = init;
}
void init_iMatrix(int iMatrix[MAXXDIM][MAXYDIM])
{
	for (int i = 0; i < MAXXDIM; i++)
		for (int j = 0; j < MAXYDIM; j++)
			iMatrix[i][j] = 0;
}

//findet den vom Betrag gr��ten/kleinsten Wert der Matrix
int  find_abs_extremum_iMatrix(int min_max, int iMatrix[MAXXDIM][MAXYDIM])
{
	int extremum = min_max == MAX ? 0 : 0xFFFF;
	for (int i = 0; i < MAXXDIM; i++)
		for (int j = 0; j < MAXYDIM; j++)
		{
			int abs = sqrt(pow(iMatrix[i][j], 2));
			if (min_max == MAX && abs > extremum)
				extremum = abs;
			else if (min_max == MIN && abs < extremum)
				extremum = abs;
		}
	return extremum;
}


void reset_blob_label(int iIMG[MAXXDIM][MAXYDIM], int oldLabel, int newLabel)
{
	for (int x = 0; x < MAXXDIM; x++)
		for (int y = 0; y < MAXYDIM; y++)
			if (iIMG[x][y] == oldLabel)
				iIMG[x][y] = newLabel;
}





void blob_coloring_imagesensitiv(unsigned char img[MAXXDIM][MAXYDIM], unsigned char img2[MAXXDIM][MAXYDIM], int iIMG[MAXXDIM][MAXYDIM], int bereich, int keine_fransen, int writeImage)
{
	init_iMatrix(iIMG);
	init_cMatrix(img2, 0);
	unsigned int blob = 0;

	for(int x = 1; x < MAXXDIM; x++)
		for (int y = 1; y < MAXYDIM; y++){
			//       Diff( x_c, x_l) > bereich                                && Diff( x_c, x_u) > bereich    -> x_c = neues Label
			if ((int)sqrt(pow(img[x][y] - img[x][y - 1], 2)) > bereich && (int)sqrt(pow(img[x][y] - img[x - 1][y], 2)) > bereich)
				iIMG[x][y] = ++blob;
			//      Diff( x_c, x_l) <= bereich                                && Diff( x_c, x_u) <= bereich   -> x_c = x_u
			else if ((int)sqrt(pow(img[x][y] - img[x][y - 1], 2)) <= bereich && (int)sqrt(pow(img[x][y] - img[x - 1][y], 2)) <= bereich ) {
					//Grauwerte sind im Intervall, aber die labels im Merker sind nicht identisch -> falsches label
					int l_extend = (bereich / 2) < 1 ? 1 : (bereich / 2);
					if (iIMG[x - 1][y] != iIMG[x][y - 1] &&	// Label stimmen nicht �berein -> Selbe Region?
						(int)sqrt(pow(img[x][y] - img[x][y - l_extend], 2)) <= bereich && // L-Maske verbreitern -> immernoch im Bereich?
						(int)sqrt(pow(img[x][y] - img[x - l_extend][y], 2)) <= bereich && // L-Maske verl�ngern -> immernoch im Bereich?
						x > l_extend && y > l_extend && keine_fransen == 1) {
						int old_label = iIMG[x - 1][y] > iIMG[x][y - 1] ? iIMG[x - 1][y] : iIMG[x][y - 1];
						int new_label = iIMG[x - 1][y] > iIMG[x][y - 1] ? iIMG[x][y - 1] : iIMG[x - 1][y];
						reset_blob_label(iIMG, old_label, new_label);
					}
				//</---Verhindern von Ausfransungen Ende--->
				// nun gew�nlich x_u, x_c zuweisen
				iIMG[x][y] = iIMG[x - 1][y];
			}
			//      Diff( x_c, x_l) <= bereich                                && Diff( x_c, x_u) > bereich
			else if ((int)sqrt(pow(img[x][y] - img[x][y - 1], 2)) <= bereich && (int)sqrt(pow(img[x][y] - img[x - 1][y], 2)) > bereich) {
				if (y == 1)
					iIMG[x][y] = ++blob;
				else
					iIMG[x][y] = iIMG[x][y - 1];
			}
			//      Diff( x_c, x_l) > bereich                                && Diff( x_c, x_u) <= bereich
			else if ((int)sqrt(pow(img[x][y] - img[x][y - 1], 2)) > bereich && (int)sqrt(pow(img[x][y] - img[x - 1][y], 2)) <= bereich) {
					iIMG[x][y] = iIMG[x - 1][y];
			}
		}
	// R�nder der Merkermatrix markieren
	for (int x = 0; x < MAXXDIM; x++)
		for (int y = 0; y < MAXYDIM; y++)
			if (!(x == 0 || y == 0 ))
				continue;
			else if (x == 0)
				iIMG[x][y] = iIMG[x + 1][y];
			else if (y == 0)
				iIMG[x][y] = iIMG[x][y + 1];

	// Ausgabebild nach dem Eingabebild einf�rben
	if (writeImage == 1){
		// anzahl der Blobs herausfinden
		int max = find_abs_extremum_iMatrix(MAX, iIMG);
		int null_labels = 0;
		for (int i = 0; i <= max; i++){
			float counter = 0, mittelwert = 0;
			for (int x = 0; x < MAXXDIM; x++) 
				for (int y = 0; y < MAXYDIM; y++)
					if (iIMG[x][y] == i) {
						mittelwert += (float)img[x][y];
						counter += 1.0;
					}
			if(counter > 0)// keine div durch 0 zulassen
				mittelwert /= counter;
			if (mittelwert != 0.0) {
				for (int x = 0; x < MAXXDIM; x++)
					for (int y = 0; y < MAXYDIM; y++)
						if (iIMG[x][y] == i)
							img2[x][y] = (unsigned char)mittelwert;
			}
			else
				null_labels++;
		}
		writeImage_ppm(img2, MAXXDIM, MAXYDIM);
		system("cls");
		printf("Anzahl der Blobs: %i", (max-null_labels));
		_getch();
		fflush(stdin);
	}
}

