#define _CRT_SECURE_NO_WARNINGS

#include "image-io.h"
#include "BagCounter.h"
#include <stdio.h>
#include"pre_processing.h"
#include<conio.h>
#include <stdlib.h>
#include<cmath>
#include <string>

void BagMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	fflush(stdin);
	char eingabe = '0';
	bool exit = false;
	//int marker[MAXXDIM][MAXYDIM];


	do {
		int anz = 0;
		int rep = 0;
		int deb = 0;
		int pixelCount = 0;
		system("cls");
		printf("\nWas wollen sie tun ?\n");

		printf("1 = Vorverarbeiten\n");

		printf("2 = Binarisieren\n");

		printf("3 = S�cke Z�hlen\n");



		printf("0 = zum Hauptmenue \n");



		printf("ihre Wahl ?:");
		fflush(stdin);

		scanf("%c", &eingabe);

		printf("\n");

		switch (eingabe) {
		case '0':
			exit = true;
			break;
		case'1':

			system("cls");

			printf("====================Median Filter ============================\n");

			printf("Welche Groesse soll der filter Haben? :");

			scanf("%i", &anz);


			if (anz > 0)

			{

				medianFilter(inImg, outImg, anz);


				writeImage_ppm(outImg, MAXXDIM, MAXYDIM);
				printf("=======================================================\n");

			}

			else

			{

				system("cls");

				printf("Eine Falsche eingabe wurde erkannt!!! \n Bitte geben sie eine Ganzahl eine welche groe�er als 0 ist\nDruecken sie eine belibige taste um in das menue zurueckzugehren ");

				_getch();

			}

			break;
		case'2':
			system("cls");
			printf("==================== Schwellwert Operation ============================\n");
			printf("Schwellwert ? :");
			scanf("%i", &anz);


			if (anz > 0)
			{
				schwellwert(inImg, outImg,anz);
			}

			break;
		case '3':
			system("cls");
			printf("==================== S�cke Z�hlen ============================\n");
			
			int verteilung[100];

			

			for (int z = 0;z<100;z++)
			{
				verteilung[z] = 0;

			}
			printf("Wie breit sollen die Streifen sein ? :");
			scanf("%i", &anz);

			printf("Soll der Ablauf angezeigt werden  ? :");
			scanf("%i", &deb);

			printf("Soll im anschluss ein Bericht erstellt werden (ja = 1 / nein = 0) ? :");
			scanf("%i", &rep);

			bool debug = false;
			if (deb == 1)
			{
				debug = true;
			}
			else
			{
				debug = false;
			}


			if (anz > 0)
			{
				int countedBag = CountBags(inImg, verteilung, anz,debug);
				float varianz = 0;// calc_Varianz(verteilung);
				float stdAbweichung = 0;// sqrt(varianz);
				printf("\n\n\n");
				printf("Es befinden sich laut der messung %i Saecke im Buendel\n", countedBag);
				//printf("Die Varianz der Messung betraegt %f\n",varianz);

				if (rep == 1 )
				{
					create_report(verteilung, countedBag);
				}
				_getch();
			}
			
			

				
			//_getch();

			break;

		}





	} while (exit == false);


}

void schwellwert(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char  out [MAXXDIM][MAXYDIM], unsigned char schwellwert)
{
	initImmage(out);
	
	for (int y = 0;y < MAXYDIM;y++)
	{
		for (int x = 0;x < MAXXDIM;x++)
		{
			if (inImg[y][x]>= schwellwert)
			{
				out[y][x] =255;
			}
			else
			{
				out[y][x] = 0;
			}
		}
		
	}
	
	My_IMGDEBUG(out);

	writeImage_ppm(out, MAXXDIM, MAXYDIM);

}

void create_report(int verteilung[100],int anzahl)
{
	FILE	*fpRep;
	char	fname[30];
	printf("\nReport Speichern::Dateiname (ohne '.ext'!) : ");
	scanf("%s", &fname);
	strcat(fname, ".txt");

	if ((fpRep = fopen(fname, "w+")) == NULL) {
		printf("Kann Datei nicht oeffnen!\n");
		
	}
	else {
		fprintf(fpRep, "Aufzeichnung der Ergebnisse der Sackb�ndelz�hlung\n");
		fprintf(fpRep, "=========================================Algemeine Daten========================================\n");
		fprintf(fpRep, "Berechnete Sackzahl \t\t\t\t ->%i\n",anzahl);
		
		fprintf(fpRep, "================================================================================================\n\n\n");


		fprintf(fpRep, "==========================H�figkeitsverteilung der einzelnen streifen===========================\n");
		int max = 0;

		for (int i = 0;i < 100;i++)
		{
			if (verteilung[i]!= 0)
			{
				max = i;
			}

		}


		for (int i = 0;i <max+10;i++)
		{
			
			fprintf(fpRep, "%i \t %i\n",i ,verteilung[i]);


		}

		
		fprintf(fpRep, "================================================================================================\n");
		

		
	}
	fclose(fpRep);

}


int CountBags(unsigned char inImg[MAXXDIM][MAXYDIM], int verteilung[100],int size,bool debug)
{
	

	int maxBagCount = 0;

	int final_Bag_count = 0;

	

	for (int c = 1; c <= MAXXDIM; c = c + size)
	{
		if (debug == true)
		{
			printf("\nNeues Segment\n");
		}

		int erg = grasfire_Streifen(inImg, c, c + size, debug);
		if (erg > maxBagCount)
		{

			maxBagCount = erg;
		}

		if (erg < 100)
		{

			verteilung[erg]++;
		}
		if (debug == true)
		{
			printf("Anzahl der Objekte im Segment = %i\n",erg);
		}

	}
	/*
	for (int z = 0;z <= maxBagCount + 10;z++)
	{
		printf("%i -> %i\n", z, verteilung[z]);

	}*/
	
	int temp = 0;
	for (int z = 0;z <= maxBagCount + 10;z++)
	{
		if (verteilung[z]>temp)
		{

			temp = verteilung[z];
			final_Bag_count = z;

		}

	}
	return final_Bag_count;
}


int grasfire_Streifen(unsigned char inImg[MAXXDIM][MAXYDIM],  unsigned char start, unsigned char stop ,bool debug)
{
	 // int  markerImg[MAXXDIM][MAXYDIM],
		// Alle Konstanten sind in der BinBV.h Definiert !!! und kommentiert 		

		bool objCountGrowing = true;
		int objCounter = 0;		int bagCounter = 0;
		int oldObjCounter = 0;

		unsigned char markerImg[MAXXDIM][MAXYDIM];
		unsigned char tempImg[MAXXDIM][MAXYDIM];

		// alles Null schreiben
		initImmage(markerImg);
		initImmage(tempImg);

		// Zuendpunkt suchen Hier while schleife weil diese veralssen werden soll wenn der punkt gefunden wurde!

		while (objCountGrowing)
			//solange die anzahl der objekte zunimmt...
		{
			int zp[2] = { 0,0 };
			bool zpFound = false;

			int zpx = 1;
			int zpy = start;
			// zuendpunkt suchen ... Wenn gefunden Abrch der schleife 
			while (zpx < MAXXDIM - 1 && zpFound == false)
			{
				zpy = start;
				while (zpy < stop - 1 && zpFound == false)
				{

					if (inImg[zpx][zpy] >= GF_TRIGGERLEVEL && tempImg[zpx][zpy] == GF_NOTUSED)
					{
						//Der zuendpunkt mus weiss sein und noch nie bearbeitet worden sein 
						zp[0] = zpx;
						zp[1] = zpy;
						zpFound = true;
					}

					zpy++;
				}
				zpx++;
			}

			bool objGrowing = true;
			int oldPixelCount = 0;
			int newPixelCount = 0;			int objSizeOld = 0;			int objSize = 0;

			if (zpFound == true)
			{
				//wenn ein zuenpunkt gefunden wurde.
				//zuendpunkt im Markierungs Bild setzen 
				markerImg[zp[0]][zp[1]] = GF_WHITE;
				// das Pixel im Temp  Bild auf in process setzen
				tempImg[zp[0]][zp[1]] = GF_INPROCESS;
				objSizeOld = glob_countWhitePixel(markerImg);
				while (objGrowing == true) // eigendlicher Grasfire 
										   //solange die das ojekt groe�er wird ..
				{

					for (int x = 1; x < MAXXDIM - 1; x++)
					{

						for (int y = start; y < stop -1; y++)
						{

							if (markerImg[x][y] >= GF_TRIGGERLEVEL && tempImg[x][y] == GF_INPROCESS)
							{
								// Delatiere mit elementarraute 
								markerImg[x][y] = GF_WHITE;
								markerImg[x + 1][y] = GF_WHITE;
								markerImg[x - 1][y] = GF_WHITE;
								markerImg[x][y + 1] = GF_WHITE;
								markerImg[x][y - 1] = GF_WHITE;



								//und Verknuepfung ... Original und markierungsbild 
								if (!(inImg[x + 1][y] >= GF_TRIGGERLEVEL &&markerImg[x][y + 1] >= GF_WHITE))
								{
									// Pixel zurueksetzen 
									markerImg[x + 1][y] = GF_BLACK;
								}
								else
								{
									if (tempImg[x + 1][y] != GF_DONE)
									{
										//wenn nicht schon abgearbeitet dann in Process setzen.
										tempImg[x + 1][y] = GF_INPROCESS;
									}
								}
								//und Verknuepfung ... Original und markierungsbild 
								if (!(inImg[x - 1][y] >= GF_TRIGGERLEVEL &&markerImg[x][y - 1] >= GF_WHITE))
								{
									// Pixel zurueksetzen 
									markerImg[x - 1][y] = GF_BLACK;
								}
								else
								{
									if (tempImg[x - 1][y] != GF_DONE)
									{
										//wenn nicht schon abgearbeitet dann in Process setzen.
										tempImg[x - 1][y] = GF_INPROCESS;
									}
								}
								//und Verknuepfung ... Original und markierungsbild 
								if (!(inImg[x][y + 1] >= GF_TRIGGERLEVEL &&markerImg[x][y + 1] >= GF_WHITE))
								{
									// Pixel zurueksetzen 
									markerImg[x][y + 1] = GF_BLACK;
								}
								else
								{
									if (tempImg[x][y + 1] != GF_DONE)
									{
										//wenn nicht schon abgearbeitet dann in Process setzen.
										tempImg[x][y + 1] = GF_INPROCESS;
									}
								}
								//und Verknuepfung ... Original und markierungsbild 
								if (!(inImg[x][y - 1] >= GF_TRIGGERLEVEL &&markerImg[x][y - 1] >= GF_WHITE))
								{
									// Pixel zurueksetzen 
									markerImg[x][y - 1] = GF_BLACK;
								}
								else
								{
									if (tempImg[x][y - 1] != GF_DONE)
									{
										//wenn nicht schon abgearbeitet dann in Process setzen.
										tempImg[x][y - 1] = GF_INPROCESS;
									}
								}

								//und Verknuepfung ... Original und markierungsbild 
								if (!(inImg[x][y] >= GF_TRIGGERLEVEL &&markerImg[x][y] >= GF_WHITE))
								{
									// Pixel zurueksetzen 
									markerImg[x][y] = GF_BLACK;
								}
								else
								{
									// pixel abgearbeitet
									tempImg[x][y] = GF_DONE;
								}

							}

						}
					}
					// nicht schwatze pixel zaehlen ..
					newPixelCount = glob_countWhitePixel(markerImg);

					if (newPixelCount > oldPixelCount)
					{
						// wenn die anzahl zugenommen hat sclaeife neusterten 
						objGrowing = true;
					}					
					else
					{

						// wenn die anzahl nicht zunimmt dann Objek abgeschlossen 
						objGrowing = false;					     objSize = glob_countWhitePixel(markerImg);
						//und objektcounter hochzaehlen
						objCounter++;																								if (Classifier((float)(objSize - objSizeOld), (stop - start)) == true)						{														bagCounter++;						}						if (debug == true)						{							printf("Size = %i \n", ((int)(objSize - objSizeOld)));							My_IMGDEBUG(markerImg);						}
					}

					oldPixelCount = newPixelCount;

				}



			}// Grasfeuer

			 // ueberpruefen ob die objektanzahl zunimmt ...
			if (objCounter>oldObjCounter)
			{
				// im Algorytmus bleiben.
				objCountGrowing = true;
			}
			else
			{
				// Algorytmus beenden
				objCountGrowing = false;

			}


			oldObjCounter = objCounter;


		}//objektschleife 
			
				return bagCounter;		//return objCounter;
				
	}

// Klasifikator 

bool Classifier(float volume, int columwith )
{
	bool erg = false;

	float hight = (float)((float)volume / (float)columwith);

	if (hight >= 1.0 && volume >= 10)
	{
		erg = true;

	}
	else
	{
		erg = false;

	}

	return erg;
}


// ===============================================Pixel count Funktion ============================================

// Die Funktion bestimmt die anzahl der nicht weissen pixel 
// Als eingabe dient ein Bild mit 256 x 256 Pixeln die ruekgabe ist eine ganzzahl.
int glob_countWhitePixel(unsigned char img[MAXXDIM][MAXYDIM])
{
	int erg = 0;

	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			if (img[x][y] != 0)
			{
				erg++;
			}
		}
	}
	return erg;
}