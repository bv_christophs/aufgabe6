#include "image-io.h"

extern void TexMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM]);
extern void energiewerte_nach_Laws(unsigned char inImg[MAXXDIM][MAXYDIM], float outImg[MAXXDIM][MAXYDIM]);
extern void showEW(float in[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM]);

extern void generate_COCcurrence_Matrix(unsigned char in[MAXXDIM][MAXYDIM], int out[MAXXDIM][MAXYDIM], int xFakt, int yFakt);
extern void show_COCcurrence_Matrix(int in[MAXXDIM][MAXYDIM], unsigned char out[MAXXDIM][MAXYDIM]);

extern void generate_COCcurrence_Matrix_add(int in[MAXXDIM][MAXYDIM], float out[MAXXDIM][MAXYDIM]);

extern void generate_COCcurrence_Matrix_devide( float out[MAXXDIM][MAXYDIM], int devider);
extern void show_COCcurrence_Matrix_gesamt(float in[MAXXDIM][MAXYDIM], unsigned char out[MAXXDIM][MAXYDIM]);


extern float asm_energie(float in[MAXXDIM][MAXYDIM]);