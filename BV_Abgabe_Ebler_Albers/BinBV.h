#include "image-io.h"

// Fuer Grassfire
#define GF_TRIGGERLEVEL  200 // Schwellwert ab wann ein pixel als weiss erkannt wird
#define GF_WHITE 255 // komplett weiss
#define GF_BLACK 0 // schwatz

#define GF_NOTUSED 0  // Das Pixel wurde noch nie bearbeitet 
#define GF_INPROCESS 125 // Das Pixel ist aktiv und wird im n�chsten durchlauf bearbeitet
#define GF_DONE 255 // Das pixel ist komplett abgearbeitet und wird nicht mehr beachtet


extern void BinMenue(unsigned char INimg[MAXXDIM][MAXYDIM], unsigned char OUTimg[MAXXDIM][MAXYDIM]);

extern void blowImmage(unsigned char INimg[MAXXDIM][MAXYDIM], unsigned char OUTimg[MAXXDIM][MAXYDIM]);
extern void shrinkImmage(unsigned char INimg[MAXXDIM][MAXYDIM], unsigned char OUTimg[MAXXDIM][MAXYDIM]);

extern int countWhitePixel(unsigned char INimg[MAXXDIM][MAXYDIM]);

extern void schliessen(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM], int wieoft);

extern void oeffnen(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM], int wieoft);

extern int grassfire_for_OBJcounting(unsigned char inImg[MAXXDIM][MAXYDIM]);