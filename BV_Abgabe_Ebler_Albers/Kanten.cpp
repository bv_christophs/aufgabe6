#define _CRT_SECURE_NO_WARNINGS

#include "image-io.h"
#include "pre_processing.h"
#include <stdio.h>
#include<conio.h>
#include <stdlib.h>
#include <string>
#include<cmath>
#include "Kanten.h"

void KantenMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	fflush(stdin);
	
	char eingabe = '0';
	bool exit = false;

	do {
		int anz = 0;
		int pixelCount = 0;
		system("cls");
		printf("\nWas wollen sie tun ?\n");

		printf("1 = Sobel X\n");

		printf("2 = Sobel Y\n");

		printf("3 = Sobel X Y \n");

		printf("4 = Laplace \n");

		printf("5 = DoG\n");

		printf("6 = \n");

		printf("0 = zum Hauptmenue \n");



		printf("ihre Wahl ?:");
		fflush(stdin);

		scanf("%c", &eingabe);


		printf("\n");

		int sobXY[MAXXDIM][MAXYDIM];
		int sobX[MAXXDIM][MAXYDIM];
		int sobY[MAXXDIM][MAXYDIM];
	


		double gaus1[MAXXDIM];
		double gaus2[MAXXDIM];
		double gaus3[MAXXDIM];

		int Size_g1;
		int Size_g2;
		int Size_g3;


		switch (eingabe) {
		case '0':
			exit = true;
			break;
		case'1':
			
			
			
			
			SobelX(inImg, sobX);
			for (int x = 0; x < MAXXDIM; x++)
			{
				for (int y = 0; y < MAXYDIM; y++)
				{
					if (sobX[x][y] != 0)
					{
						outImg[x][y] =255;
					}
					else
					{
						outImg[x][y] = 0;
					}
				}
			}
			My_IMGDEBUG(outImg);

			break;
		case '2':
			
			
			
			SobelY(inImg, sobY);

			for (int x = 0; x < MAXXDIM; x++)
			{
				for (int y = 0; y < MAXYDIM; y++)
				{
					if (sobY[x][y] != 0)
					{
						outImg[x][y] = 255;
					}
					else
					{
						outImg[x][y] = 0;
					}
				}
			}
			My_IMGDEBUG(outImg);


			break;
		case '3':
			

			SobelXY(inImg, sobX, sobY, sobXY);

			for (int x = 0; x < MAXXDIM; x++)
			{
				for (int y = 0; y < MAXYDIM; y++)
				{
					if (sobXY[x][y] != 0)
					{
						outImg[x][y] = 255;
					}
					else
					{
						outImg[x][y] = 0;
					}
				}
			}
			My_IMGDEBUG(outImg);

			break;

		case '4':
			initImmageGray(outImg);

			laplace(inImg, sobXY);

			for (int x = 0; x < MAXXDIM; x++)
			{
				for (int y = 0; y < MAXYDIM; y++)
				{
					if (sobXY[x][y] >0)
					{
						outImg[x][y] = 255;
					}
					if (sobXY[x][y] < 0)
					{
						outImg[x][y] = 0;
					}
				}
			}
			My_IMGDEBUG(outImg);

			break;
		case '5':

			system("cls");
			printf("====================Difference of Gausian============================\n");
			printf("Welche Groesse soll der filter Haben? :");
			scanf("%i", &anz);
			


			if (anz > 0)
			{

				
				Size_g1 = genaerate_Gaus(anz, gaus1);
				Size_g2 = genaerate_Gaus(anz + 2, gaus2);
				Size_g3 = subGaus(Size_g2, gaus2, gaus1, gaus3);
				useGausfilter(Size_g3, gaus3, inImg, outImg);

				printf("=======================================================\n");
			}
			else
			{
				system("cls");
				printf("Eine Falsche eingabe wurde erkannt!!! \n Bitte geben sie eine Ganzahl eine welche groe�er als 0 ist\nDruecken sie eine belibige taste um in das menue zurueckzugehren ");
				_getch();
			}

			

			_getch();
			break;

		case '6':

			break;
		}





	} while (exit == false);


}

void SobelX(unsigned char inImg[MAXXDIM][MAXYDIM],int outImg[MAXXDIM][MAXYDIM])
{
	
	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			outImg[x][y] = 0;

		}
	}

	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			outImg[x][y]  = 1 * inImg[x - 1][y - 1] + -1 * inImg[x - 1][y + 1]
				+ 2 * inImg[x][y - 1] + -2 * inImg[x][y + 1]
				+ 1 * inImg[x + 1][y - 1] + -1 * inImg[x + 1][y + 1];

			
			 
		}
	}
}

void SobelY(unsigned char inImg[MAXXDIM][MAXYDIM], int outImg[MAXXDIM][MAXYDIM])
{

	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			outImg[x][y] = 0;

		}
	}

	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			outImg[x][y] = 1 * inImg[x - 1][y - 1] + 2 * inImg[x - 1][y] + 1 * inImg[x - 1][y + 1] +
				          -1 * inImg[x + 1][y - 1] - 2 * inImg[x + 1][y] - 1 * inImg[x + 1][y + 1];
				


		}
	}
}

void SobelXY(unsigned char inImg[MAXXDIM][MAXYDIM], int sobX[MAXXDIM][MAXYDIM], int sobY[MAXXDIM][MAXYDIM],int out[MAXXDIM][MAXYDIM])
{
	

	

	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			
			out[x][y] = 0;

		}
	}


	SobelX(inImg, sobX);
	SobelY(inImg, sobY);

	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			out[x][y] = (int)pow((pow(sobX[x][y], 2)+ pow(sobY[x][y], 2)),0.5);
		}
	}

}


void laplace(unsigned char inImg[MAXXDIM][MAXYDIM], int outImg[MAXXDIM][MAXYDIM])
{

	for (int x = 0; x < MAXXDIM; x++)
	{
		for (int y = 0; y < MAXYDIM; y++)
		{
			outImg[x][y] = 0;

		}
	}

	for (int x = 1; x < MAXXDIM; x++)
	{
		for (int y = 1; y < MAXYDIM; y++)
		{
			outImg[x][y] = 1*inImg[x-1][y]+ 1 * inImg[x ][y-1] + 1 * inImg[x][y + 1] + 1 * inImg[x +1][y ]  -4 * inImg[x][y];
		}
	}
}

int fakultaet(int zahl)
{
	int erg = 1;
	int i = 1;
	for (int i = 1 ; i<=zahl;i++)
	{
		erg = erg * i;
		
	}
	return erg;
}
// diese Funktion erzeugt eine binomenalverteilung mit der gr��e size
int genaerate_Gaus(int size,double gaus[MAXXDIM])
{
	
	for (int i = 0; i < MAXXDIM;i++)
	{

		gaus[i] = 0;
	}
	
	//Gau�verteilung erstellen 
	for (int i = 0; i < size;i++)
	{
		gaus[i] = (double)(fakultaet(size-1)) / (double)(fakultaet(i)*fakultaet(size-1 - i));
		
	}

	// Normalisieren
	double sum = 0;
	for (int i = 0; i < size;i++)

	{
		sum = (double)sum + gaus[i];
	}
	
	for (int i = 0; i < size;i++)
	{
		gaus[i] = (double)(gaus[i]/(double)sum);
	}
	/*
	printf("---------\n");
	for (int i = 0; i < size;i++)
	{
		printf("%f \n", gaus[i]);
	}
	printf("---------\n");
	*/
	
	return size;
}

int subGaus(int size, double plus[MAXXDIM], double minus[MAXXDIM],double out[MAXXDIM])
{

	
	for (int i = 0; i < MAXXDIM;i++)
	{
		out[i] = 0;
	}

	for (int i = 0; i < size;i++)
	{
		if ( i == 0)
		{
			out[i] = plus[i] - 0;
		}
		else
		{
			out[i] = plus[i] - minus[i-1];
		}
	}
	

	return size;
	
}

void useGausfilter(int size,double gaus[MAXXDIM], unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	initImmageGray(outImg);
	double SummeWerte = 0;
	double min = 0, max = 0, faktor = 0;

	int i, j, k, l;
	int arraySize = MAXXDIM*MAXYDIM;
	int sizehalbe = size*0.5;



	for (i = 0; i < MAXXDIM - size; i++) {
		for (j = 0; j < MAXYDIM - size; j++) {
			SummeWerte = 0;

			for (int x = 0; x < size; x++)
			{
				for (int y = 0; y < size; y++)
				{
					SummeWerte = SummeWerte + (gaus[x] * gaus[y] * (double)inImg[i + x][j + y]);
				}
			}
			if (SummeWerte < min) {
				min = SummeWerte;
			}

			if (SummeWerte > max) {
				max = SummeWerte;
			}
		}

	}

	//Faktor berechnen
	faktor = (double)MAXXDIM / (max - min);



	for (i = 0; i < MAXXDIM - size; i++) {
		for (j = 0; j < MAXYDIM - size; j++) {

			SummeWerte = 0;

			for (int x = 0; x < size; x++)
			{
				for (int y = 0; y < size; y++)
				{
					SummeWerte = SummeWerte + ((gaus[x] * gaus[y]) * (double)inImg[i + x][j + y]);
				}
			}

			outImg[i + sizehalbe][j + sizehalbe] = (unsigned char)((double)(SummeWerte*faktor) + 128);
		}

	}


	writeImage_ppm(outImg, MAXXDIM, MAXYDIM);

	
}