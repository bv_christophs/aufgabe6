#include "image-io.h"
extern void SegMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM]);

extern void blob_colorring_grayscale(unsigned char inImg[MAXXDIM][MAXYDIM], int markerImg[MAXXDIM][MAXYDIM], unsigned char schwellwert);
extern void blob_colorring(unsigned char inImg[MAXXDIM][MAXYDIM], int markerImg[MAXXDIM][MAXYDIM], unsigned char schwellwert);
extern void replaceLable(int marker[MAXXDIM][MAXYDIM], int oldLable, int newLable);
extern int  find_ex( int iMatrix[MAXXDIM][MAXYDIM]);