#define _CRT_SECURE_NO_WARNINGS

#include "image-io.h"
#include "Texturen.h"
#include <stdio.h>
#include<conio.h>
#include <stdlib.h>
#include<cmath>
#include <string>

void TexMenue(unsigned char inImg[MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	fflush(stdin);
	char eingabe = '0';
	bool exit = false;
	float ew[MAXXDIM][MAXYDIM];
	int coM[MAXXDIM][MAXYDIM];
	float coMall[MAXXDIM][MAXYDIM];
	

	do {
		int anz = 0;
		int anz1 = 0;
		int pixelCount = 0;
		system("cls");
		printf("\nWas wollen sie tun ?\n");

		printf("1 = Energiewerte\n");

		printf("2 = Cooccurrence- Gesmat Matrix\n");

	

		//	printf("6 = \n");

		printf("0 = zum Hauptmenue \n");



		printf("ihre Wahl ?:");
		fflush(stdin);

		scanf("%c", &eingabe);

		printf("\n");

		switch (eingabe) {
		case '0':
			exit = true;
			break;
		case'1':
			energiewerte_nach_Laws(inImg, ew);
			showEW(ew, outImg);
			break;
		case '2':

			for (int x = 0;x<MAXXDIM;x++)
			{
				for (int y = 0;y < MAXXDIM;y++)
				{

					coMall[x][y] = 0;
				}
			}

			//0 Grad
			generate_COCcurrence_Matrix(inImg, coM, 0, 1);
			generate_COCcurrence_Matrix_add(coM, coMall);
		

			//45 Grad
			generate_COCcurrence_Matrix(inImg, coM, -1, 1);
			generate_COCcurrence_Matrix_add(coM, coMall);
			

			//90 Grad
			generate_COCcurrence_Matrix(inImg, coM, -1, 0);
			generate_COCcurrence_Matrix_add(coM, coMall);
			

			//135 Grad
			generate_COCcurrence_Matrix(inImg, coM, -1, -1);
			generate_COCcurrence_Matrix_add(coM, coMall);


			generate_COCcurrence_Matrix_devide(coMall, 4);

			printf("Berechnete ASM Energie: %f", asm_energie(coMall));
			printf("\nDruecken sie eine beliebige taste um das Ausgabe Bild zu Speichern");
			_getch();

			show_COCcurrence_Matrix_gesamt(coMall, outImg);
			
			break;
		case '3':

			
			break;

		case '4':
		

			break;
		case '5':

			
			break;

		case '6':

			break;
		}





	} while (exit == false);


}


void energiewerte_nach_Laws(unsigned char inImg[MAXXDIM][MAXYDIM], float outI[MAXXDIM][MAXYDIM])
{
	
	int L3L3;
	int L3E3;
	int L3S3;

	int E3L3;
	int E3E3;
	int E3S3;

	int S3L3;
	int S3E3;
	int S3S3;

	for (int x = 1; x < MAXXDIM -1; x++)
	{
		for (int y = 1; y < MAXYDIM-1; y++)
		{
			L3L3 = 0;

			//L3L3 = 1*(inImg[x-1][y-1]) + 2*(inImg[x-1][y]) + 1*(inImg[x-1][y+1]) + 2*(inImg[x][y-1]) + 4*(inImg[x][y]) + 2*(inImg[x][y+1]) + 1*(inImg[x+1][y-1]) + 2*(inImg[x-1][y]) + 1*(inImg[x-1][y+1]);

			L3E3 = -1*(inImg[x - 1][y - 1]) + 0*(inImg[x - 1][y]) + 1*(inImg[x - 1][y + 1]) + -2*(inImg[x][y - 1]) + 0*(inImg[x][y]) + 2*(inImg[x][y + 1]) + -1*(inImg[x + 1][y - 1]) + 0*(inImg[x - 1][y]) + 1*(inImg[x - 1][y + 1]);

			L3S3 = -1*(inImg[x - 1][y - 1]) + 2*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]) + -2*(inImg[x][y - 1]) + 4*(inImg[x][y]) + -2*(inImg[x][y + 1]) + -1*(inImg[x + 1][y - 1]) + 2*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]);

			
			E3L3 =  -1*(inImg[x - 1][y - 1]) + 2*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]) + 0*(inImg[x][y - 1]) + 0*(inImg[x][y]) + 0*(inImg[x][y + 1]) + 1*(inImg[x + 1][y - 1]) + -2*(inImg[x - 1][y]) + 1*(inImg[x - 1][y + 1]);

			E3E3 =  1*(inImg[x - 1][y - 1]) + 0*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]) + 0*(inImg[x][y - 1]) + 0*(inImg[x][y]) + 0*(inImg[x][y + 1]) + -1*(inImg[x + 1][y - 1]) + 2*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]);

			E3S3 =  1*(inImg[x - 1][y - 1]) + -2*(inImg[x - 1][y]) + 1*(inImg[x - 1][y + 1]) + 0*(inImg[x][y - 1]) + 0*(inImg[x][y]) + 0*(inImg[x][y + 1]) + -1*(inImg[x + 1][y - 1]) + 2*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]);



			S3L3 =  -1*(inImg[x - 1][y - 1]) - 2*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]) + 2*(inImg[x][y - 1]) + 4*(inImg[x][y]) + 2*(inImg[x][y + 1]) + -1*(inImg[x + 1][y - 1]) + -2*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]);

			S3E3 =  1*(inImg[x - 1][y - 1]) + 0*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]) + -2*(inImg[x][y - 1]) + 0*(inImg[x][y]) + 2*(inImg[x][y + 1]) + 1*(inImg[x + 1][y - 1]) + 0*(inImg[x - 1][y]) + -1*(inImg[x - 1][y + 1]);

			S3S3 =  1*(inImg[x - 1][y - 1]) + -2*(inImg[x - 1][y]) + 1*(inImg[x - 1][y + 1]) + -2*(inImg[x][y - 1]) + 4*(inImg[x][y]) + -2*(inImg[x][y + 1]) + 1*(inImg[x + 1][y - 1]) + -2*(inImg[x - 1][y]) + 1*(inImg[x - 1][y + 1]);
		
		
			outI[x][y] = (float) sqrt( pow(L3L3, 2) + pow(L3E3, 2) + pow(L3S3, 2) + pow(E3L3, 2) + pow(E3E3, 2) + pow(E3S3, 2) + pow(S3L3, 2) + pow(S3E3, 2) + pow(S3S3, 2)  );
		
		}
	}


}

void showEW(float in [MAXXDIM][MAXYDIM], unsigned char outImg[MAXXDIM][MAXYDIM])
{
	initImmage(outImg);
	//My_IMGDEBUG(outImg);
	float maxVal = 0;
	float minVal = 255;

	for (int x = 1; x < MAXXDIM - 1; x++)
	{
		for (int y = 1; y < MAXYDIM - 1; y++)
		{
			if (in[x][y] > maxVal)
			{
				maxVal = in[x][y];
			}

			if (in[x][y] < minVal)
			{
				minVal = in[x][y];
			}

		}
	}

	float faktor = 255 / maxVal;
	for (int x = 1; x < MAXXDIM - 1; x++)
	{
		for (int y = 1; y < MAXYDIM - 1; y++)
		{
			outImg[x][y] = (unsigned char)((float)faktor*(float)(in[x][y] -minVal));
		}
	}
	
	My_IMGDEBUG(outImg);
}


void generate_COCcurrence_Matrix(unsigned char in [MAXXDIM][MAXYDIM],int out [MAXXDIM][MAXYDIM],int xFakt,int yFakt)
{
	for (int x = 0;x<MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{

			out[x][y] = 0;
		}
	}

	for (int x = 0;x<MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{

			out[in[x][y]][in[x + (xFakt)][y + (yFakt)]] += 1;
			out[in[x + (xFakt)][y + (yFakt)]][in[x][y]] += 1;

		}
	}
}

void show_COCcurrence_Matrix( int in[MAXXDIM][MAXYDIM], unsigned char out [MAXXDIM][MAXYDIM])
{
	initImmage(out);

	int max = 0;

	for (int x = 0;x < MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{
			if (in[x][y] >max)
			{
				max = in[x][y];
			}

		}

	}

	
	float fakto = (float)((float)MAXXDIM / (float)max);
	for (int x = 0;x < MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{
			
			if (in[x][y] >0)
			{
				out[x][y] = 255;
			}
			
			//out[x][y] = fakto*in[x][y];

		}

	}
	//writeImage_ppm(out,MAXXDIM,MAXYDIM);
	My_IMGDEBUG(out);
}


void show_COCcurrence_Matrix_gesamt(float in[MAXXDIM][MAXYDIM], unsigned char out[MAXXDIM][MAXYDIM])
{

	initImmageWhite(out);

	float max = 0;

	for (int x = 0;x < MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{
			if (in[x][y] >max)
			{
				max = in[x][y];
			}

		}

	}


	float fakto = (float)((float)MAXXDIM / (float)max);
	for (int x = 0;x < MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{

			if (in[x][y] >0)
			{
			//	out[x][y] = 0;
			}

			out[x][y] =255- (unsigned char)(fakto* (float) in[x][y]);

		}

	}


	writeImage_ppm(out, MAXXDIM, MAXYDIM);


}

void generate_COCcurrence_Matrix_add(int in[MAXXDIM][MAXYDIM],float out[MAXXDIM][MAXYDIM])
{
	


	for (int x = 0;x < MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{
			out[x][y] +=in[x][y];
		}
	}

}

void generate_COCcurrence_Matrix_devide(float out[MAXXDIM][MAXYDIM], int devider)
{

	for (int x = 0;x < MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{
			(float)out [x][y]  = (float)out[x][y] / (float)devider;
		}
	}
}

float asm_energie(float in[MAXXDIM][MAXYDIM])
{
	float erg = 0;
	float ges = 0;

	for (int x = 0;x < MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{
			ges += in[x][y];
		}
	}

	for (int x = 0;x < MAXXDIM;x++)
	{
		for (int y = 0;y < MAXXDIM;y++)
		{
			erg  += pow(((float)in[x][y]/(float)ges),2);
		}
	}

	return erg;

}